package co.simplon.restaurant.entity;

public class Plat {

    int id;
    String name;
    int prix;
    
    public Plat() {
    }
    public Plat(String name, int prix) {
        this.name = name;
        this.prix = prix;
    }
    public Plat(int id, String name, int prix) {
        this.id = id;
        this.name = name;
        this.prix = prix;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getPrix() {
        return prix;
    }
    public void setPrix(int prix) {
        this.prix = prix;
    }
    


}
