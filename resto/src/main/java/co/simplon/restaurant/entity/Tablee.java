package co.simplon.restaurant.entity;

import java.time.LocalDateTime;

public class Tablee {

    private Integer id;
    private LocalDateTime date;
    private String nomClient;
    private Integer chaise;

    
    public Tablee() {
    }
    public Tablee(LocalDateTime date, String nomClient, Integer chaise) {
        this.date = date;
        this.nomClient = nomClient;
        this.chaise = chaise;
    }
    public Tablee(Integer id, LocalDateTime date, String nomClient, Integer chaise) {
        this.id = id;
        this.date = date;
        this.nomClient = nomClient;
        this.chaise = chaise;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public LocalDateTime getDate() {
        return date;
    }
    public void setDate(LocalDateTime date) {
        this.date = date;
    }
    public String getNomClient() {
        return nomClient;
    }
    public void setNomClient(String nomClient) {
        this.nomClient = nomClient;
    }
    public Integer getChaise() {
        return chaise;
    }
    public void setChaise(Integer chaise) {
        this.chaise = chaise;
    }
    
    
    

    
}
