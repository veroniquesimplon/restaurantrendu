package co.simplon.restaurant.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import co.simplon.restaurant.entity.Commande;
import co.simplon.restaurant.entity.Formule;
import co.simplon.restaurant.entity.Plat;

public class FormuleRepository {
    public List<Formule> findAll() {
        List<Formule> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM formule");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Formule formule = new Formule(
                    result.getInt("id"),
                    result.getString("name"),
                    result.getInt("prix")
                    );
                    list.add(formule);
            }
        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }
        return list;
    }
    

    public List<Commande> findAllCommandes(int id) {
        List<Commande> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT * FROM  commande INNER JOIN commande_formule ON commande_formule.id_commande=commande.id  WHERE commande_formule.id_formule=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Commande commande = new Commande(
                        result.getInt("id"),
                        result.getString("NomClient"),
                        result.getInt("total"));
                list.add(commande);
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return list;
    }

    public List<Plat> findAllPlats(int id) {
        List<Plat> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT*FROM plat INNER JOIN formule_plat ON formule_plat.id_plat=plat.id WHERE formule_plat.id_formule=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Plat plat = new Plat(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getInt("prix"));
                list.add(plat);
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return list;
    }


    public Formule findById(int id) {
        try (Connection connection = Database.connect()) {            
            PreparedStatement stmt = 
            connection.prepareStatement("SELECT * FROM formule WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Formule(
                    result.getInt("id"), 
                    result.getString("name"),
                    result.getInt("prix"));
            }
        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }
        return null;
    } 

    public boolean delete(int id) {

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM formule WHERE id=?");
            stmt.setInt(1, id);
            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }

        return false;
    }

    public boolean persist(Formule formule) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO formule (name, prix) VALUES (?, ?)" , Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, formule.getName());
            stmt.setInt(2, formule.getPrix());
            
            if(stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                formule.setId(keys.getInt(1));
                return true;
            }
            
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }
        return false;
    }

    public boolean update(Formule formule ){
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE formule SET name=?, prix=?   WHERE id=?;");
            stmt.setString(1, formule.getName());
            stmt.setInt(2, formule.getPrix());
            stmt.setInt(3, formule.getId());
            
            if(stmt.executeUpdate() == 1) {
                return true;
            }
    
            
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }
        return false;
    }
}
