package co.simplon.restaurant.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import co.simplon.restaurant.entity.Tablee;

public class TableeRepository {

    public List<Tablee> findAll() {
        List<Tablee> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {            
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM tablee");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Tablee tablee = new Tablee(
                    result.getInt("id"),
                    result.getTimestamp("date").toLocalDateTime(),
                    result.getString("nom_client"),
                    result.getInt("chaise"));
                    list.add(tablee);
                
            }
        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }
        return list;
    }

    public Tablee findById(int id) {
        try (Connection connection = Database.connect()) {            
            PreparedStatement stmt = 
            connection.prepareStatement("SELECT * FROM tablee WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Tablee(
                    result.getInt("id"), 
                    result.getTimestamp("date").toLocalDateTime(),
                    result.getString("nom_client"),
                    result.getInt("chaise"));
            }
        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }
        return null;
    } 
    
    public boolean delete(int id) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM tablee WHERE id=?");
            stmt.setInt(1, id);
            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }

        return false;
    }

    public boolean persist(Tablee tablee) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO tablee (date, nom_client, chaise) VALUES (?, ?, ?)" , Statement.RETURN_GENERATED_KEYS);
            stmt.setTimestamp(1, Timestamp.valueOf( tablee.getDate()));
            stmt.setString(2, tablee.getNomClient());
            stmt.setInt(3, tablee.getChaise());
            
            if(stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                tablee.setId(keys.getInt(1));
                return true;
            }
            
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }
        return false;
    }


    public boolean update(Tablee tablee ){
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE tablee SET date=? , nom_client=?, chaise=?  WHERE id=?;");
            stmt.setTimestamp(1, Timestamp.valueOf( tablee.getDate()));
            stmt.setString(2, tablee.getNomClient());
            stmt.setInt(3, tablee.getChaise());
            stmt.setInt(4, tablee.getId());
            
            if(stmt.executeUpdate() == 1) {
                return true;
            }
    
            
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }
        return false;
    }

}
