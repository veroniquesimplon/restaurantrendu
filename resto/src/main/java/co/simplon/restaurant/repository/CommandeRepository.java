package co.simplon.restaurant.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import co.simplon.restaurant.entity.Commande;
import co.simplon.restaurant.entity.Formule;
import co.simplon.restaurant.entity.Plat;

public class CommandeRepository {
    public List<Formule> findAllFormules(int id) {
        List<Formule> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT * FROM formule INNER JOIN commande_formule ON commande_formule.id_formule=formule.id  WHERE commande_formule.id_commande=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Formule formule = new Formule(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getInt("prix"));
                list.add(formule);
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return list;
    }

    public List<Plat> findAllPlat(int id) {
        List<Plat> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT * FROM plat INNER JOIN plat_commande ON plat_commande.id_plat=plat.id WHERE plat_commande.id_commande=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Plat plat = new Plat(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getInt("prix"));
                list.add(plat);
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return list;
    }

    public List<Commande> findAll() {
        List<Commande> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM commande");
            ResultSet result = stmt.executeQuery();
            while (result.next()){
                Commande commande = new Commande(
                result.getInt("id"),
                result.getString("nom_client"),
                result.getInt("total"));
                list.add(commande);
            }
        System.out.println("liste crée");
        } catch (SQLException e) {
            System.out.println("error from repository");
            e.printStackTrace();
        }
        return list;
    }


    public Commande findById(int id) {

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM commande WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Commande(
                        result.getInt("id"),
                        result.getString("nom_client"),
                        result.getInt("total"));
            }
        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }

        return null;
    }

    public boolean persist(Commande commande) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO commande (nom_client,total) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, commande.getNomClient());
            stmt.setInt(2, commande.getTotal());

            if (stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                commande.setId(keys.getInt(1));
                return true;
            }

        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }
        return false;
    }

    public boolean update(Commande commande) {

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection
                    .prepareStatement("UPDATE commande SET nom_client=?, total=? WHERE id=?;");
            stmt.setString(1, commande.getNomClient());
            stmt.setInt(2, commande.getTotal());
            stmt.setInt(3, commande.getId());
            if (stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }
        return false;
    }

    public boolean delete(int id) {

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM commande WHERE id=?");
            stmt.setInt(1, id);
            if (stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }

        return false;
    }

}
