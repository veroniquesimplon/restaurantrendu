package co.simplon.restaurant.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import co.simplon.restaurant.entity.Commande;
import co.simplon.restaurant.entity.Formule;
import co.simplon.restaurant.entity.Ingredients;
import co.simplon.restaurant.entity.Plat;

public class PlatRepository {
    public List<Commande> findAllCommandes(int id) {
        List<Commande> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT * FROM commande INNER JOIN plat_commande ON plat_commande.id_commande=commande.id WHERE plat_commande.id_plat=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Commande commande = new Commande(
                        result.getInt("id"),
                        result.getString("NomClient"),
                        result.getInt("total"));
                list.add(commande);
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return list;
    }

    public List<Formule> findAllFormules(int id) {
        List<Formule> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement(
                    " SELECT*FROM formule INNER JOIN formule_plat ON formule_plat.id_formule=formule.id WHERE formule_plat.id_plat=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Formule formule = new Formule(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getInt("prix"));
                list.add(formule);
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return list;
    }

    public List<Ingredients> findAllIngredients(int id) {
        List<Ingredients> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT*FROM ingredient INNER JOIN ingredient_plat ON ingredient_plat.id_ingredient=ingredient.id WHERE ingredient_plat.id_plat=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Ingredients ingredients = new Ingredients(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getInt("stock"));
                list.add(ingredients);
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return list;
    }

    public List<Plat> findAll() {

        List<Plat> list = new ArrayList<>();

        try (Connection connection = Database.connect()) {

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM plat");

            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Plat plat = new Plat(

                        result.getInt("id"),
                        result.getString("name"),
                        result.getInt("prix"));

                list.add(plat);

            }

            System.out.println("liste crée");
        } catch (SQLException e) {
            System.out.println("error from repository");
            e.printStackTrace();
        }

        return list;

    }

    public Plat findById(int id) {

        try (Connection connection = Database.connect()) {

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM plat WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if (result.next()) {
                return new Plat(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getInt("prix"));
            }

        } catch (SQLException e) {

            e.printStackTrace();
        }

        return null;
    }

    public Boolean delete(int id) {

        try (Connection connection = Database.connect()) {

            PreparedStatement stmt = connection.prepareStatement("DELETE FROM plat WHERE id=?");
            stmt.setInt(1, id);

            int lignesdelete = stmt.executeUpdate();

            if (lignesdelete == 1) {
                return true;
            } else {
                return false;
            }

        } catch (SQLException e) {
            e.printStackTrace();

            return false;
        }
    }

    public Boolean persist(Plat plat) {

        try (Connection connection = Database.connect()) {

            PreparedStatement stmt = connection.prepareStatement("INSERT INTO plat (name,prix) VALUES (?,?)",
                    Statement.RETURN_GENERATED_KEYS);

            stmt.setString(1, plat.getName());
            stmt.setInt(2, plat.getPrix());

            int ligneajoutee = stmt.executeUpdate();

            if (ligneajoutee == 1) {

                ResultSet generatedKeys = stmt.getGeneratedKeys();
                generatedKeys.next();
                int idGenere = generatedKeys.getInt(1);
                System.out.println("ID généré pour le nouveau plat : " + idGenere);

                return true;
            }

            else {
                return false;
            }

        } catch (SQLException e) {

            e.printStackTrace();
            return false;
        }
    }

    public Boolean update(Plat plat) {

        try (Connection connection = Database.connect()) {

            PreparedStatement stmt = connection.prepareStatement("UPDATE plat SET name=?,prix=? WHERE Id = ?");
            stmt.setString(1, plat.getName());
            stmt.setInt(2, plat.getPrix());
            stmt.setInt(3, plat.getId());

            int changed = stmt.executeUpdate();

            if (changed == 1) {
                System.out.println("success");
                return true;
            } else {
                System.out.println("raté");
                return false;
            }

        } catch (SQLException e) {
            e.printStackTrace();

        }

        return false;
    }

}
