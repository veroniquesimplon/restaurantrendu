package co.simplon.restaurant.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import co.simplon.restaurant.entity.Ingredients;
import co.simplon.restaurant.entity.Plat;

public class IngredientRepository {
    public List<Plat> findAllPlat(int id) {
        List<Plat> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement(
                    " SELECT*FROM plat INNER JOIN ingredient_plat ON ingredient_plat.id_plat=plat.id WHERE ingredient_plat.id_ingredient=? ");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Plat plat = new Plat(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getInt("prix"));
                list.add(plat);
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return list;
    }

    public List<Ingredients> findAll() {

        List<Ingredients> list = new ArrayList<>();

        try (Connection connection = Database.connect()) {

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM ingredient");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Ingredients ingredient = new Ingredients(

                        result.getInt("id"),
                        result.getString("name"),
                        result.getInt("stock"));

                list.add(ingredient);

            }

            System.out.println("liste crée");
        } catch (SQLException e) {
            System.out.println("error from repository");
            e.printStackTrace();
        }

        return list;

    }

    public Ingredients findById(int id) {

        try (Connection connection = Database.connect()) {

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM ingredient WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if (result.next()) {
                return new Ingredients(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getInt("stock"));
            }

        } catch (SQLException e) {

            e.printStackTrace();
        }
        System.out.println("raté");
        return null;
    }

    public Boolean delete(int id) {

        try (Connection connection = Database.connect()) {

            PreparedStatement stmt = connection.prepareStatement("DELETE FROM ingredient WHERE id=?");
            stmt.setInt(1, id);

            int lignesdelete = stmt.executeUpdate();

            if (lignesdelete == 1) {
                return true;
            } else {
                return false;
            }

        } catch (SQLException e) {
            e.printStackTrace();

            return false;
        }
    }

    public Boolean persist(Ingredients ingredient) {

        try (Connection connection = Database.connect()) {

            PreparedStatement stmt = connection.prepareStatement("INSERT INTO ingredient (name,stock) VALUES (?,?)",
                    Statement.RETURN_GENERATED_KEYS);

            stmt.setString(1, ingredient.getName());
            stmt.setInt(2, ingredient.getStock());

            int ligneajoutee = stmt.executeUpdate();

            if (ligneajoutee == 1) {

                ResultSet generatedKeys = stmt.getGeneratedKeys();
                generatedKeys.next();
                int idGenere = generatedKeys.getInt(1);
                System.out.println("ID généré pour le nouvel ingrédient : " + idGenere);

                return true;
            }

            else {
                return false;
            }

        } catch (SQLException e) {

            e.printStackTrace();
            return false;
        }
    }

    public Boolean update(Ingredients ingredient) {

        try (Connection connection = Database.connect()) {

            PreparedStatement stmt = connection.prepareStatement("UPDATE ingredient SET name=?,stock=? WHERE id = ?");
            stmt.setString(1, ingredient.getName());
            stmt.setInt(2, ingredient.getStock());
            stmt.setInt(3, ingredient.getId());

            int changed = stmt.executeUpdate();

            if (changed == 1) {
                System.out.println("success");
                return true;
            } else {
                System.out.println("raté");
                return false;
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }

    }

}
