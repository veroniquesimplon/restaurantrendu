package co.simplon.restaurant.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import co.simplon.restaurant.entity.Commande;

public class CommandeRepositoryTest {

    @BeforeEach
    void setUp() {
        try {
            ScriptRunner runner = new ScriptRunner(Database.connect());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Test
    void findAllShouldReturnAListOfCommande() {
        CommandeRepository repo = new CommandeRepository();
        List<Commande> result = repo.findAll();

        assertEquals(10, result.size());
        assertEquals("Jean Dupont", result.get(0).getNomClient());
        assertEquals(40, result.get(0).getTotal());
        assertEquals(1, result.get(0).getId());

        assertNotNull(result.get(0).getNomClient());
        assertNotNull(result.get(0).getTotal());
        assertNotNull(result.get(0).getId());
    }

    @Test
    void findByIdWithResult() {
        CommandeRepository repo = new CommandeRepository();
        Commande result = repo.findById(1);

        assertEquals("Jean Dupont", result.getNomClient());
        assertEquals(40, result.getTotal());
        assertEquals(1, result.getId());
    }

    @Test
    void deleteSuccess() {
        CommandeRepository repo = new CommandeRepository();
        boolean result = repo.delete(1);
        assertTrue(result);
        assertEquals(9, repo.findAll().size());
    }

    @Test
    void persistSuccess() {
        CommandeRepository repo = new CommandeRepository();
        Commande mimi = new Commande ("Cheikh", 19);
        boolean result = repo.persist(mimi);
        assertTrue(result);

        assertEquals(11, repo.findAll().size());
    }

    @Test
    void updateSuccess() {
        CommandeRepository repo = new CommandeRepository();
        Commande commande = new Commande ( 1,"Souad", 10 );
        assertTrue(repo.update(commande));
        Commande updated = repo.findById(1);
        assertEquals(10, updated.getTotal());
    }


}
