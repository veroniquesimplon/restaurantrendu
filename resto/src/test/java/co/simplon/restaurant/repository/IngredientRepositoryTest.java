package co.simplon.restaurant.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import co.simplon.restaurant.entity.Ingredients;

public class IngredientRepositoryTest {

    @BeforeEach
    void setUp() {
        try {
            ScriptRunner runner = new ScriptRunner(Database.connect());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    @Test
    void findAllShouldReturnAListOfIngredient() {
        IngredientRepository repo = new IngredientRepository();
        List<Ingredients> result = repo.findAll();

        
        assertEquals(38, result.size());
        assertEquals("pomme de terre", result.get(0).getName());
        assertEquals(1, result.get(0).getId());
        assertNotNull(result.get(0).getName());        
        assertNotNull(result.get(0).getId());
    }

    

        
    @Test
    void findByIdResult() {
        IngredientRepository repo = new IngredientRepository();
        Ingredients result = repo.findById(1);
        assertEquals("pomme de terre", result.getName());
        assertEquals(15, result.getStock());
        assertEquals(1, result.getId());
    } 


    @Test
    void findByIdNoResult() {
        IngredientRepository repo = new IngredientRepository();
        Ingredients result = repo.findById(1000);
        assertNull(result);
    }

    @Test
    void deleteSuccess() {
        IngredientRepository repo = new IngredientRepository();
        boolean result = repo.delete(1);
        assertTrue(result);
        assertEquals(37, repo.findAll().size());
    }

    @Test
    void persistSuccess() {
        IngredientRepository repo = new IngredientRepository();
        Ingredients ingredient = new Ingredients("Scmlork",22);
        repo.persist(ingredient);
        assertTrue(repo.persist(ingredient));
        assertEquals(22, ingredient.getStock());
        assertEquals(40, repo.findAll().size());
    }

    
    @Test
    void updateSuccess() {
        IngredientRepository repo = new IngredientRepository();
        Ingredients ingredient = new Ingredients( 2, "lettuce",8);
        repo.update(ingredient);
        assertTrue(repo.update(ingredient));        
        Ingredients updated = repo.findById(2);
        assertEquals("lettuce", updated.getName());
        assertEquals(8, updated.getStock());
    }



}
