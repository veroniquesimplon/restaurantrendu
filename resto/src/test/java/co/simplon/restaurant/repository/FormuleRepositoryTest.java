package co.simplon.restaurant.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import co.simplon.restaurant.entity.Formule;

public class FormuleRepositoryTest {

    @BeforeEach
    void setUp() {
        try {
            ScriptRunner runner = new ScriptRunner(Database.connect());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    @Test
    void findAllShouldReturnAListOfFormules() {
        FormuleRepository repo = new FormuleRepository();
        List<Formule> result = repo.findAll();

        assertEquals(10, result.size());
        assertEquals("Menu Pizza", result.get(0).getName());
        assertEquals(15, result.get(0).getPrix());
        assertEquals(1, result.get(0).getId());

        assertNotNull(result.get(0).getName());
        assertNotNull(result.get(0).getPrix());
        assertNotNull(result.get(0).getId());
    }


    @Test
    void findByIdWithResult() {
        FormuleRepository repo = new FormuleRepository();
        Formule result = repo.findById(1);

        assertEquals("Menu Pizza", result.getName());
        assertEquals(15, result.getPrix());
        assertEquals(1, result.getId());
    }

    @Test
    void deleteSuccess() {
        FormuleRepository repo = new FormuleRepository();

        boolean result = repo.delete(1);
        assertTrue(result);

        assertEquals(9, repo.findAll().size());
    }

    @Test
    void persistSuccess() {
        FormuleRepository repo = new FormuleRepository();
        Formule mimi = new Formule ("Menu Pizza", 15);
        boolean result = repo.persist(mimi);
        assertTrue(result);

        assertEquals(11, repo.findAll().size());
    }


    @Test
    void updateSuccess() {
        FormuleRepository repo = new FormuleRepository();
        Formule formule = new Formule ( 1,"Souad", 10 );
        assertTrue(repo.update(formule));
    
        Formule updated = repo.findById(1);
        assertEquals(10, updated.getPrix());
    }
}
