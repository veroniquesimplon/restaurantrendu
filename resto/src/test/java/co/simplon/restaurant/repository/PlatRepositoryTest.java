package co.simplon.restaurant.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import co.simplon.restaurant.entity.Plat;

public class PlatRepositoryTest {

    @BeforeEach
    void setUp() {
        try {
            ScriptRunner runner = new ScriptRunner(Database.connect());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
@Test
    void findAllShouldReturnAListOfPlat() {
        PlatRepository repo = new PlatRepository();
        List<Plat> result = repo.findAll();        
        assertEquals(14, result.size());
        assertEquals("poulet aux olives", result.get(0).getName());
        assertEquals(20, result.get(0).getPrix());
        

        assertNotNull(result.get(0).getName());
        assertNotNull(result.get(0).getId());

    }

    

        
    @Test
    void findByIdResult() {
        PlatRepository repo = new PlatRepository();
        Plat result = repo.findById(1);
        assertEquals("poulet aux olives", result.getName());
        assertEquals(1, result.getId());
    } 


    @Test
    void findByIdNoResult() {
        PlatRepository repo = new PlatRepository();
        Plat result = repo.findById(1000);

        assertNull(result);
    }

    @Test
    void deleteSuccess() {
        PlatRepository repo = new PlatRepository();

        boolean result = repo.delete(1);
        assertTrue(result);
        assertEquals(13, repo.findAll().size()); 
    }


    @Test
    void persistSuccess() {
        PlatRepository repo = new PlatRepository();
        Plat plat = new Plat("Schmlork", 10);
        repo.persist(plat);
        assertTrue(repo.persist(plat));
        assertEquals("Schmlork", plat.getName());


    }

    
    @Test
    void updateSuccess() {
        PlatRepository repo = new PlatRepository();
        Plat plat = new Plat( 1, "Royal with cheese",10);
        repo.update(plat);
        assertTrue(repo.update(plat));
        
        Plat updated = repo.findById(1);
        
        assertEquals("Royal with cheese", updated.getName());
    }

}
